Veeam тестовое приложение
=========================

Введение
--------
Простое приложение, использующее:

- ZF2
- Doctrine 2
- capistrano
- composer
- twitter bootstrap

для реализации тестового задания компании Veeam.

Установка
---------

- Клонируем репозиторий:

```
#!console
    git clone git@bitbucket.org:yurakovalev/veeam.git veeam
```

- Создаём базу данных в MySQL (в примере используется `veeam`, `uft8_general_ci`).

- Устанавливаем зависимости:

```
#!console
    cd veeam/
    ./composer.phar install
```

- Создаём файл `/config/autoload/doctrine.local.php` и помещаем в него следующее содержимое (где указываем необходимые для подключения к базе данных параметры (создавалась на предыдущих шагах)):

```
#!php
    <?php

    return array(
        'doctrine' => array(
            'connection' => array(
                'orm_default' => array(
                    'driverClass' =>'Doctrine\DBAL\Driver\PDOMySql\Driver',
                    'params' => array(
                        'host'     => 'localhost',
                        'port'     => '3306',
                        'user'     => 'root',
                        'password' => 'root',
                        'dbname'   => 'veeam',
                    )
                )
            )
        )
    );
```

- Запускаем генерацию структуры базы данных на основе моделей:

```
#!console
    ./vendor/bin/doctrine-module orm:schema-tool:create
    ATTENTION: This operation should not be executed in a production environment.

    Creating database schema...
    Database schema created successfully!
```

- Записываем в таблицы базы данных тестовые значения:

```
#!console
    mysql -u root -proot veeam < /data/dump.sql
```

- Настраиваем корень веб-сервера на `veeam/public`

- Готово!