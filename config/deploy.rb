require 'capistrano_colors'

set :stage_dir, 'config/deploy'
set :stages, %w[production]
set :default_stage, "production"

set :application, "veeam"
set :host_path, "/home/virtwww/w_veeam-test-iurii-r_ba19d9c7/http"
set :deploy_to, "#{host_path}/#{application}"

# SCM
set :scm, 'git'
set :repository, "git@bitbucket.org:yurakovalev/veeam.git"
set :deploy_via, :remote_cache
set :copy_exclude, [".git"]

set :use_sudo, false
set :user, "w_veeam-test-iurii-r_ba19d9c7"

set :keep_releases, 3
set :normalize_asset_timestamps, false

default_run_options[:pty] = true

namespace :my do
  task :copy_config, :roles => :app do
    run "cp #{shared_path}/doctrine.local.php #{release_path}/config/autoload/doctrine.local.php"
  end
  task :run_composer, :roles => :app do
    run "/usr/local/bin/php -d detect_unicode=Off #{release_path}/composer.phar install"
  end
end

namespace :deploy do
  before "deploy:create_symlink", "my:copy_config"
  before "deploy:create_symlink", "my:run_composer"
  after "deploy:restart", "deploy:cleanup"
end

require 'capistrano/ext/multistage'

namespace :multistage do
 task :prepare do
 end
end
