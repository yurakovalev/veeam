SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;


INSERT INTO `department` (`id`, `title`) VALUES
(1, 'Отдел разработки'),
(2, 'Отдел маркетинга'),
(3, 'Отдел продаж'),
(4, 'Топ-менеджмент');

INSERT INTO `lang` (`lang_id`) VALUES
('en'),
('fr'),
('it'),
('ru');

INSERT INTO `vacancy` (`id`, `department_id`) VALUES
(1, 1),
(2, 1),
(3, 2),
(4, 2),
(5, 3),
(6, 3),
(7, 4),
(8, 4);

INSERT INTO `vacancy__lang` (`lang_id`, `vacancy_id`, `title`, `description`) VALUES
('en', 1, 'Frontend developer', 'Frontend developer description'),
('en', 2, 'Backend developer', 'Backend developer description'),
('en', 3, 'Marketing Manager', 'Marketing Manager description'),
('en', 4, 'Brand Manager', 'Brand Manager description'),
('en', 5, 'Sales Manager', 'Sales Manager description'),
('en', 6, 'Head of Direct Sales', 'Head of Direct Sales description'),
('en', 7, 'CEO', 'CEO description'),
('en', 8, 'CTO', 'CTO description'),
('ru', 2, 'Бекенд разработчик', 'Описание вакансии бекенд разработчика'),
('ru', 4, 'Бренд менеджер', 'Описание вакансии бренд менеджера'),
('ru', 5, 'Менеджер по продажам', 'Описание вакансии менеджера по продажам');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
