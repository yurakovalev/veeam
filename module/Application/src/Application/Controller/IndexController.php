<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Application;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Doctrine\ORM\EntityManager;
use Application\Entity\Vacancy;

class IndexController extends AbstractActionController
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * Sets the EntityManager
     *
     * @param EntityManager $em
     * @access protected
     * @return IndexController
     */
    protected function setEntityManager(EntityManager $em)
    {
        $this->entityManager = $em;
        return $this;
    }

    /**
     * Returns the EntityManager
     *
     * Fetches the EntityManager from ServiceLocator if it has not been initiated
     * and then returns it
     *
     * @access protected
     * @return EntityManager
     */
    protected function getEntityManager()
    {
        if (null === $this->entityManager) {
            $this->setEntityManager($this->getServiceLocator()->get('Doctrine\ORM\EntityManager'));
        }

        return $this->entityManager;
    }

    /**
     * Index action
     *
     * @return array|ViewModel
     */
    public function indexAction()
    {
        $departments = $this->getEntityManager()
            ->getRepository('Application\Entity\Department')
            ->findAll();

        $langs = $this->getEntityManager()
            ->getRepository('Application\Entity\Lang')
            ->findAll();

        $departmentId = null;
        $langId = null;

        $request = $this->getRequest();

        if ($request->isPost()) {
            $post = $request->getPost();
            $departmentId = (int) $post['departmentId'];
            $langId = $post['langId'];

            $result = Vacancy::getByDepartmentAndLang(
                $this->getEntityManager(),
                $departmentId,
                $langId
            );
        } else {
            // By default get all vacancies
            $result = Vacancy::getAll(
                $this->getEntityManager()
            );
        }

        return new ViewModel(array(
            'departments' => $departments,
            'langs' => $langs,
            'result' => $result,
            'departmentId' => $departmentId,
            'langId' => $langId
        ));
    }
}
