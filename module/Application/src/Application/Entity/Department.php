<?php

namespace Application\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Department
 *
 * @ORM\Entity
 * @ORM\Table(name="department")
 */
class Department {
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     */
    private $title;

    /**
     * Returns the Identifier
     *
     * @access public
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets the Title
     *
     * @param string $title
     * @access public
     * @return Department
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * Returns the Title
     *
     * @access public
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }
}