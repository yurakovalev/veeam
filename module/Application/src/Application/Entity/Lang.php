<?php

namespace Application\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Lang
 *
 * @ORM\Entity
 * @ORM\Table(name="lang")
 */
class Lang {
    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(name="lang_id", type="string", length=2, nullable=false)
     */
    private $id;

    /**
     * Returns the Identifier
     *
     * @access public
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
}