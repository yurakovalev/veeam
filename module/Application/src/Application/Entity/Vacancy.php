<?php

namespace Application\Entity;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;

/**
 * Vacancy
 *
 * @ORM\Entity
 * @ORM\Table(name="vacancy")
 */
class Vacancy {
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="department_id", type="integer", nullable=false)
     */
    private $departmentId;

    /**
     * Get list of vacancies by department and lang
     *
     * @param EntityManager $em
     * @param integer $departmentId
     * @param string $langId
     * @param string $defaultLangId
     * @return array
     */
    public static function getByDepartmentAndLang(EntityManager $em, $departmentId, $langId, $defaultLangId = 'en')
    {
        $query = $em->createQuery("
                SELECT v.id,
                  d.id as department_id,
                  d.title as department_title,
                  (CASE WHEN (vl1.title IS NOT NULL) THEN vl1.title ELSE vl2.title END) as vacancy_title,
                  (CASE WHEN (vl1.description IS NOT NULL) THEN vl1.description ELSE vl2.description END) as vacancy_description
                FROM Application\Entity\Vacancy as v
                    LEFT JOIN Application\Entity\Department as d WITH v.departmentId = d.id
                    LEFT JOIN Application\Entity\VacancyLang as vl1 WITH vl1.vacancyId = v.id AND vl1.langId = :langId
                    LEFT JOIN Application\Entity\VacancyLang as vl2 WITH vl2.vacancyId = v.id AND vl2.langId = :defaultLangId
                WHERE v.departmentId = :departmentId
        ");

        $query->setParameters(array(
            'langId' => $langId,
            'departmentId' => $departmentId,
            'defaultLangId' => $defaultLangId
        ));

        return $query->getResult();
    }

    /**
     * Get all vacancies
     *
     * @param EntityManager $em
     * @param string $langId
     * @return array
     */
    public static function getAll(EntityManager $em, $langId = 'en')
    {
        $query = $em->createQuery("
                SELECT v.id,
                  d.id as department_id,
                  d.title as department_title,
                  vl.title as vacancy_title,
                  vl.description as vacancy_description
                FROM Application\Entity\Vacancy as v
                    LEFT JOIN Application\Entity\Department as d WITH v.departmentId = d.id
                    LEFT JOIN Application\Entity\VacancyLang as vl WITH vl.vacancyId = v.id AND vl.langId = :langId
        ");

        $query->setParameters(array(
            'langId' => $langId,
        ));

        return $query->getResult();
    }
}