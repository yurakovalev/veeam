<?php

namespace Application\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * VacancyLang
 *
 * @ORM\Entity
 * @ORM\Table(name="vacancy__lang")
 */
class VacancyLang {
    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(name="lang_id", type="string", length=2, nullable=false)
     */
    private $langId;

    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(name="vacancy_id", type="integer", nullable=false)
     */
    private $vacancyId;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=false)
     */
    private $description;
}